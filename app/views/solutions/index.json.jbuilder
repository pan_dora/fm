json.array!(@solutions) do |solution|
  json.extract! solution, :id, :name, :description, :status
  json.url solution_url(solution, format: :json)
end
