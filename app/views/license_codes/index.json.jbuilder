json.array!(@license_codes) do |license_code|
  json.extract! license_code, :id, :code, :description, :status
  json.url license_code_url(license_code, format: :json)
end
