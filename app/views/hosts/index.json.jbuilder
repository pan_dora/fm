json.array!(@hosts) do |host|
  json.extract! host, :id, :name, :ip, :dbname, :username, :password, :port, :address, :status
  json.url host_url(host, format: :json)
end
