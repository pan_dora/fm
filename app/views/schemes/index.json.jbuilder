json.array!(@schemes) do |scheme|
  json.extract! scheme, :id, :name, :version, :description, :status, :attach_id
  json.url scheme_url(scheme, format: :json)
end
