class Api::V1::AttachesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def show
    attach = Attach.find(params[:id])
    render json: Api::V1::AttachSerializer.new(attach).to_json
  end
end
