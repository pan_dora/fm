class Api::V1::ToolsController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    tools = Tool.all
    render json: ActiveModel::ArraySerializer.new(tools, each_serializer: Api::V1::ToolSerializer, root: 'tools')
  end

  def show
    tool = Tool.find(params[:id])
    render json: Api::V1::ToolSerializer.new(tool).to_json
  end
end
