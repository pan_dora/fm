class ProductHistoriesController < ApplicationController
  authorize_resource
  before_action :set_product_history, only: [:show, :edit, :update, :destroy]
  before_action :set_examiner, only: [:update]
  after_action :backto_product, only: [:update]

  # GET /product_histories
  # GET /product_histories.json
  def index
    @product_histories = ProductHistory.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /product_histories/1
  # GET /product_histories/1.json
  def show
  end

  # GET /product_histories/new
  def new
    @product_history = ProductHistory.new
  end

  # GET /product_histories/1/edit
  def edit
  end

  # POST /product_histories
  # POST /product_histories.json
  def create
    @product_history = ProductHistory.new(product_history_params)

    respond_to do |format|
      if @product_history.save
        format.html { redirect_to @product_history, notice: 'Product history was successfully created.' }
        format.json { render :show, status: :created, location: @product_history }
      else
        format.html { render :new }
        format.json { render json: @product_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_histories/1
  # PATCH/PUT /product_histories/1.json
  def update
    respond_to do |format|
      if @product_history.update(product_history_params)
        format.html { redirect_to @product_history, notice: 'Product history was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_history }
      else
        format.html { render :edit }
        format.json { render json: @product_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_histories/1
  # DELETE /product_histories/1.json
  def destroy
    @product_history.destroy
    respond_to do |format|
      format.html { redirect_to product_histories_url, notice: 'Product history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def examine
    @product_history = ProductHistory.find(params[:id])
    if @product_history
      @product_history.do_examine(current_user)
      redirect_to product_histories_url, notice: 'Successfully examined.'
    end
  end

  def unexamine
    @product_history = ProductHistory.find(params[:id])
    if @product_history
      @product_history.do_unexamine(current_user)
      redirect_to product_histories_url, notice: 'Successfully unexamined.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_history
      @product_history = ProductHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_history_params
      params.require(:product_history).permit(:description, :status, :operater_id, :examiner_id, :product_id, :firmware_id, :scheme_id, :upgrade_id, :tool_id)
    end

    def set_examiner
      params[:product_history][:examiner_id] = current_user[:id]
    end

    def backto_product
      if @product_history.status == 1
        @product = @product_history.product
        @product.firmware_id = @product_history.firmware_id
        @product.scheme_id = @product_history.scheme_id
        @product.upgrade_id = @product_history.upgrade_id
        @product.tool_id = @product_history.tool_id
        @product.save
      end
    end
end
