class FirmwaresController < ApplicationController
  authorize_resource
  before_action :set_firmware, only: [:show, :edit, :update, :destroy]
  before_action :uploading, only: [:create, :update]
  after_action :stored_download_log, only: [:download]

  # GET /firmwares
  # GET /firmwares.json
  def index
    @firmwares = Firmware.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /firmwares/1
  # GET /firmwares/1.json
  def show
  end

  # GET /firmwares/new
  def new
    @firmware = Firmware.new
  end

  # GET /firmwares/1/edit
  def edit
  end

  # POST /firmwares
  # POST /firmwares.json
  def create
    @firmware = Firmware.new(firmware_params)

    respond_to do |format|
      if @firmware.save
        format.html { redirect_to @firmware, notice: 'Firmware was successfully created.' }
        format.json { render :show, status: :created, location: @firmware }
      else
        format.html { render :new }
        format.json { render json: @firmware.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /firmwares/1
  # PATCH/PUT /firmwares/1.json
  def update
    respond_to do |format|
      if @firmware.update(firmware_params)
        format.html { redirect_to @firmware, notice: 'Firmware was successfully updated.' }
        format.json { render :show, status: :ok, location: @firmware }
      else
        format.html { render :edit }
        format.json { render json: @firmware.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /firmwares/1
  # DELETE /firmwares/1.json
  def destroy
    @firmware.destroy
    respond_to do |format|
      format.html { redirect_to firmwares_url, notice: 'Firmware was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Download firmware file
  def download
    @firmware = Firmware.find(params[:id])
    if (!download_file @firmware.attach)
      redirect_to firmwares_url, notice: 'No such file.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_firmware
      @firmware = Firmware.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def firmware_params
      params.require(:firmware).permit(:name, :version, :description, :status, :attach_id)
    end

    def uploading
      upload = params[:firmware][:upload_file]
      if upload && current_user
        @attach = Attach.new(upload, current_user[:id])
        if @attach.save
          params[:firmware][:attach_id] = @attach.id
        end
      end
    end

    def stored_download_log
      ary = []
      ary << "固件：#{@firmware.name}"
      ary << "名称：#{@firmware.attach.original_name}" if @firmware.attach
      ary << "URL：#{@firmware.attach.file_url}" if @firmware.attach
      ary << "HASH：#{@firmware.attach.file_hash}" if @firmware.attach
      ary << "大小：#{@firmware.attach.size / 1024 + 1} KB" if @firmware.attach
    
      if ary.size > 0
        title = "下载文件"
        content = ary * ", "
        Syslog.stored_log(title, content)
      end
    end
end
