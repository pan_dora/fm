class ProductsController < ApplicationController
  authorize_resource
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :get_cspfstu, only: [:new, :edit, :index]
  after_action :set_history, only: [:create, :update]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :model, :place, :param, :custom, :status, :category_id, :protocol_id, :solution_id)
    end

    def get_cspfstu
      @categories = Category.all
      @solutions = Solution.all
      @protocols = Protocol.all
      @firmwares = Firmware.all
      @schemes = Scheme.all
      @tools = Tool.all
      @upgrades = Upgrade.all
    end

    def set_history
      history = ProductHistory.new
      history.product_id  = @product[:id]
      history.firmware_id = params[:product][:firmware_id]
      history.scheme_id   = params[:product][:scheme_id]
      history.operater_id = current_user[:id]
      history.status = 0
      
      firmware_diff = history.firmware_id > 0 && history.firmware_id != @product.firmware_id if history.firmware_id
      scheme_diff = history.scheme_id > 0 && history.scheme_id != @product.scheme_id if history.scheme_id
      upgrade_diff = history.upgrade_id > 0 && history.upgrade_id != @product.upgrade_id if history.upgrade_id
      tool_diff = history.tool_id > 0 && history.tool_id != @product.tool_id if history.tool_id
      
      if firmware_diff || scheme_diff || upgrade_diff || tool_diff
        history.save
      end
    end
end
