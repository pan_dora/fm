class BurnHistoriesController < ApplicationController
  authorize_resource
  before_action :set_burn_history, only: [:show, :edit, :update, :destroy]

  # GET /burn_histories
  # GET /burn_histories.json
  def index
    @burn_histories = BurnHistory.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /burn_histories/1
  # GET /burn_histories/1.json
  def show
  end

  # GET /burn_histories/new
  def new
    @burn_history = BurnHistory.new
  end

  # GET /burn_histories/1/edit
  def edit
  end

  # POST /burn_histories
  # POST /burn_histories.json
  def create
    @burn_history = BurnHistory.new(burn_history_params)

    respond_to do |format|
      if @burn_history.save
        format.html { redirect_to @burn_history, notice: 'Burn history was successfully created.' }
        format.json { render :show, status: :created, location: @burn_history }
      else
        format.html { render :new }
        format.json { render json: @burn_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /burn_histories/1
  # PATCH/PUT /burn_histories/1.json
  def update
    respond_to do |format|
      if @burn_history.update(burn_history_params)
        format.html { redirect_to @burn_history, notice: 'Burn history was successfully updated.' }
        format.json { render :show, status: :ok, location: @burn_history }
      else
        format.html { render :edit }
        format.json { render json: @burn_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /burn_histories/1
  # DELETE /burn_histories/1.json
  def destroy
    @burn_history.destroy
    respond_to do |format|
      format.html { redirect_to burn_histories_url, notice: 'Burn history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_burn_history
      @burn_history = BurnHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def burn_history_params
      params.require(:burn_history).permit(:description, :operater_id, :product_id, :firmware_id, :scheme_id)
    end
end
