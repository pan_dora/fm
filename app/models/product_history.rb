class ProductHistory < ActiveRecord::Base
  STATUS_TYPE = { "未审核" => 0, "已审核" => 1, "审核失败" => -1 }

  belongs_to :operater, class_name: "User"
  belongs_to :examiner, class_name: "User"
  belongs_to :product
  belongs_to :firmware
  belongs_to :scheme
  belongs_to :upgrade
  belongs_to :tool

  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def do_examine(user)
    if save_back_to_product
      examine(user)
    end
  end

  def do_unexamine(user)
    examine(user, -1)
  end


  private

  # status 
  # 1  means: examine
  # -1 means: unexamine
  # 0  means: resume to initilizer
  def examine(user, status = 1)
    if user
      self.status = status
      self.examiner_id = user[:id]
      self.updated_at = Time.new
      self.save
    end
  end
  
  def save_back_to_product
    if self.product
      self.product.firmware_id = self.firmware_id
      self.product.scheme_id   = self.scheme_id
      self.product.upgrade_id  = self.upgrade_id
      self.product.tool_id     = self.tool_id
      self.product.save
      return true
    end
    reutrn false
  end

  def stored_create_log
    ary = []
    ary << "产品：#{self.product.model if self.product}"
    ary << "固件：#{self.firmware.name if self.firmware}"
    ary << "配置：#{self.scheme.name if self.scheme}"
    ary << "操作人：#{self.operater.name if self.operater}"
    
    if ary.size > 0
      title = "关联申请"
      content = "[#{self.product.model}] " + ary * ", "
      Syslog.stored_log(title, content, self.operater_id)
    end
  end

  def stored_update_log
    old = ProductHistory.find(self.id)
    ary = []
    ary << "产品：#{old.product.model if old.product} -> #{self.product.model if self.product}" if old.product_id != self.product_id
    ary << "固件：#{old.firmware.name if old.firmware} -> #{self.firmware.name if self.firmware}" if old.firmware_id != self.firmware_id
    ary << "配置：#{old.scheme.name if old.scheme} -> #{self.scheme.name if self.scheme}" if old.scheme_id != self.scheme_id
    ary << "操作人：#{old.operater.name if old.operater} -> #{self.operater.name if self.operater}" if old.operater_id != self.operater_id
    ary << "审核人：#{old.examiner.name if old.examiner} -> #{self.examiner.name if self.examiner}" if old.examiner_id != self.examiner_id
    
    if ary.size > 0
      title = "关联审核"
      content = "[#{old.product.model}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除关联申请", "名称：#{self.product.model if self.product}")
  end
end
