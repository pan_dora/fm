class Product < ActiveRecord::Base
  CUSTOM_TYPE = { "定制版" => true, "标准版" => false }
  
  belongs_to :category
  belongs_to :protocol
  belongs_to :solution
  belongs_to :firmware
  belongs_to :scheme
  belongs_to :upgrade
  belongs_to :tool
  
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  ## 逻辑上的代码，放到model中好像也不太对。。。。
  ## 另外每个模型中的这些代码都几乎相似的，如何处理好它呢？
  def stored_create_log
    Syslog.stored_log("添加产品", "型号：#{self.model}")
  end

  def stored_update_log
    old = Product.find(self.id)
    ary = []
    ary << "名称：#{old.name} -> #{self.name}" if old.name != self.name
    ary << "型号：#{old.model} -> #{self.model}" if old.model != self.model
    ary << "产地：#{old.place} -> #{self.place}" if old.place != self.place
    ary << "参数：#{old.param} -> #{self.param}" if old.param != self.param
    ary << "定制：#{old.custom} -> #{self.custom}" if old.custom != self.custom
    ary << "系列：#{old.category.name if old.category} -> #{self.category.name if self.category}" if old.category_id != self.category_id
    ary << "协议：#{old.protocol.name if old.protocol} -> #{self.protocol.name if self.protocol}" if old.protocol_id != self.protocol_id
    ary << "方案：#{old.solution.name if old.solution} -> #{self.solution.name if self.solution}" if old.solution_id != self.solution_id
    ary << "关联固件：#{old.firmware.name if old.firmware} -> #{self.firmware.name if self.firmware}" if old.firmware_id != self.firmware_id
    ary << "关联配置：#{old.scheme.name if old.scheme} -> #{self.scheme.name if self.scheme}" if old.scheme_id != self.scheme_id
    
    if ary.size > 0
      title = "修改产品"
      content = "[#{old.model}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除产品", "名称：#{self.model}")
  end
end
