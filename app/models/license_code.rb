class LicenseCode < ActiveRecord::Base
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def stored_create_log
    Syslog.stored_log("添加SN代号", "代号：#{self.code}，描述：#{self.description}")
  end

  def stored_update_log
    old = LicenseCode.find(self.id)
    ary = []
    ary << "名称：#{old.code} -> #{self.code}" if old.code != self.code
    ary << "描述：#{old.description} -> #{self.description}" if old.description != self.description
    
    if ary.size > 0
      title = "修改SN代号"
      content = "[#{old.code}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除SN代号", "名称：#{self.name}")
  end
end
