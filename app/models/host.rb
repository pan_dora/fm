class Host < ActiveRecord::Base
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def stored_create_log
    ary = []
    ary << "名称：#{self.name}" if self.name
    ary << "IP: #{self.ip}" if self.ip
    ary << "Dbname: #{self.dbname}" if self.dbname
    ary << "Username: #{self.username}" if self.username
    ary << "Password: #{self.password}" if self.password
    ary << "Port: #{self.port}" if self.port
    ary << "Addredd: #{self.address}" if self.address
    
    if ary.size > 0
      title = "添加服务器信息"
      content = ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_update_log
    old = Host.find(self.id)
    ary = []
    ary << "名称：#{old.name} -> #{self.name}" if self.name != old.name
    ary << "IP: #{old.ip} -> #{self.ip}" if self.ip != old.ip
    ary << "Dbname: #{old.dbname} -> #{self.dbname}" if self.dbname != old.dbname
    ary << "Username: #{old.username} -> #{self.username}" if self.username != old.username
    ary << "Password: #{old.password} -> #{self.password}" if self.password != old.password
    ary << "Port: #{old.port} -> #{self.port}" if self.port != old.port
    ary << "Addredd: #{old.address} -> #{self.address}" if self.address != old.address
    
    if ary.size > 0
      title = "修改服务器信息"
      content = "[#{old.name}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除服务器信息", "名称：#{self.name}")
  end
end
