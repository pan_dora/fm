class Upgrade < ActiveRecord::Base
  belongs_to :attach
  
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def stored_create_log
    title = "添加升级包"
    content = "名称：#{self.name}，固件版本：#{self.firmware_version}，配置版本：#{self.scheme_version}，描述：#{self.description}"
    Syslog.stored_log(title, content)
  end

  def stored_update_log
    old = Upgrade.find(self.id)
    ary = []
    ary << "名称：#{old.name} -> #{self.name}" if old.name != self.name
    ary << "固件版本：#{old.firmware_version} -> #{self.firmware_version}" if old.firmware_version != self.firmware_version
    ary << "配置版本：#{old.scheme_version} -> #{self.scheme_version}" if old.scheme_version != self.scheme_version
    ary << "描述：#{old.description} -> #{self.description}" if old.description != self.description
    
    if ary.size > 0
      title = "修改升级包"
      content = "[#{old.name}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除升级包", "名称：#{self.name}")
  end
end
