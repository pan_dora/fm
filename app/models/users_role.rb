# pretty much exclusively created for our fixtures to work
class UsersRole < ActiveRecord::Base
  belongs_to :user
  belongs_to :role
  
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def self.refresh(user_id, role_id)
    old = self.find_by_user_id(user_id)
    if old
      return self.find_by_sql("UPDATE users_roles SET role_id=#{role_id} WHERE user_id=#{user_id}")
    else
      return self.find_by_sql("INSERT into users_roles(user_id, role_id) VALUES(#{user_id}, #{role_id})")
    end
  end



  def stored_create_log
    ary = []
    ary << "用户名：#{self.user.name}" if self.user_id
    ary << "角色：#{self.role.name}" if self.role_id
    
    if ary.size > 0
      title = "关联角色"
      content = ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_update_log
    old = UsersRole.find_by_user_id(self.user_id)
    ary = []
    ary << "用户名：#{old.user.name if old.user} -> #{self.user.name if self.user}" if old.user_id != self.user_id
    ary << "角色：#{old.role.name if old.role} -> #{self.role.name if self.role}" if old.role_id != self.role_id
    
    if ary.size > 0
      title = "修改关联角色"
      content = ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("取消关联角色", "名称：#{self.user.name if self.user}")
  end
end
