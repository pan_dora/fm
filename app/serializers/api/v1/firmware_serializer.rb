class Api::V1::FirmwareSerializer < Api::V1::BaseSerializer
  attributes :id, :name, :version, :attach
  
  has_one :attach
end
