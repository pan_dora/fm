class Api::V1::BurnHistorySerializer < ActiveModel::Serializer
  attributes :id, :operater, :product, :firmware, :scheme

  has_one :operater
  has_one :product
  has_one :firmware
  has_one :scheme
end
