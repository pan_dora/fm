class Api::V1::UserSerializer < Api::V1::BaseSerializer
  attributes :id, :username, :name, :email, :created_at, :updated_at
end
