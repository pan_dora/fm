class Api::V1::LicenseCodeSerializer < Api::V1::BaseSerializer
  attributes :id, :code, :description
end
