class AddOriginalNameToAttach < ActiveRecord::Migration
  def change
    add_column :attaches, :original_name, :string
  end
end
