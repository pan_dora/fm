class CreateSchemes < ActiveRecord::Migration
  def change
    create_table :schemes do |t|
      t.string :name
      t.string :version
      t.text :description
      t.integer :status
      t.references :attach, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
