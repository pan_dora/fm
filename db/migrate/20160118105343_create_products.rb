class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :model
      t.string :place
      t.string :param
      t.boolean :custom
      t.integer :status
      t.references :category, index: true, foreign_key: true
      t.references :protocol, index: true, foreign_key: true
      t.references :solution, index: true, foreign_key: true
      t.references :firmware, index: true, foreign_key: true
      t.references :scheme, index: true, foreign_key: true
      t.references :upgrade, index: true, foreign_key: true
      t.references :tool, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
