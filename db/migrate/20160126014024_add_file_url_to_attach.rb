class AddFileUrlToAttach < ActiveRecord::Migration
  def change
    add_column :attaches, :file_url, :string
  end
end
