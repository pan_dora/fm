class CreateProtocols < ActiveRecord::Migration
  def change
    create_table :protocols do |t|
      t.string :name
      t.text :description
      t.integer :status

      t.timestamps null: false
    end
  end
end
