class CreateSolutions < ActiveRecord::Migration
  def change
    create_table :solutions do |t|
      t.string :name
      t.text :description
      t.integer :status

      t.timestamps null: false
    end
  end
end
