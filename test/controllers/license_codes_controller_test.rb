require 'test_helper'

class LicenseCodesControllerTest < ActionController::TestCase
  setup do
    @license_code = license_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:license_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create license_code" do
    assert_difference('LicenseCode.count') do
      post :create, license_code: { code: @license_code.code, description: @license_code.description, status: @license_code.status }
    end

    assert_redirected_to license_code_path(assigns(:license_code))
  end

  test "should show license_code" do
    get :show, id: @license_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @license_code
    assert_response :success
  end

  test "should update license_code" do
    patch :update, id: @license_code, license_code: { code: @license_code.code, description: @license_code.description, status: @license_code.status }
    assert_redirected_to license_code_path(assigns(:license_code))
  end

  test "should destroy license_code" do
    assert_difference('LicenseCode.count', -1) do
      delete :destroy, id: @license_code
    end

    assert_redirected_to license_codes_path
  end
end
