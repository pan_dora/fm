require 'test_helper'

class AttachesControllerTest < ActionController::TestCase
  setup do
    @attach = attaches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attaches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attach" do
    assert_difference('Attach.count') do
      post :create, attach: { download_count: @attach.download_count, extension: @attach.extension, file_hash: @attach.file_hash, file_name: @attach.file_name, file_type: @attach.file_type, save_name: @attach.save_name, save_path: @attach.save_path, size: @attach.size }
    end

    assert_redirected_to attach_path(assigns(:attach))
  end

  test "should show attach" do
    get :show, id: @attach
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attach
    assert_response :success
  end

  test "should update attach" do
    patch :update, id: @attach, attach: { download_count: @attach.download_count, extension: @attach.extension, file_hash: @attach.file_hash, file_name: @attach.file_name, file_type: @attach.file_type, save_name: @attach.save_name, save_path: @attach.save_path, size: @attach.size }
    assert_redirected_to attach_path(assigns(:attach))
  end

  test "should destroy attach" do
    assert_difference('Attach.count', -1) do
      delete :destroy, id: @attach
    end

    assert_redirected_to attaches_path
  end
end
