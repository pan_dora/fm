require 'test_helper'

class ProductHistoriesControllerTest < ActionController::TestCase
  setup do
    @product_history = product_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_history" do
    assert_difference('ProductHistory.count') do
      post :create, product_history: { description: @product_history.description, examiner_id: @product_history.examiner_id, firmware_id: @product_history.firmware_id, operater_id: @product_history.operater_id, product_id: @product_history.product_id, scheme_id: @product_history.scheme_id, status: @product_history.status, tool_id: @product_history.tool_id, upgrade_id: @product_history.upgrade_id }
    end

    assert_redirected_to product_history_path(assigns(:product_history))
  end

  test "should show product_history" do
    get :show, id: @product_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_history
    assert_response :success
  end

  test "should update product_history" do
    patch :update, id: @product_history, product_history: { description: @product_history.description, examiner_id: @product_history.examiner_id, firmware_id: @product_history.firmware_id, operater_id: @product_history.operater_id, product_id: @product_history.product_id, scheme_id: @product_history.scheme_id, status: @product_history.status, tool_id: @product_history.tool_id, upgrade_id: @product_history.upgrade_id }
    assert_redirected_to product_history_path(assigns(:product_history))
  end

  test "should destroy product_history" do
    assert_difference('ProductHistory.count', -1) do
      delete :destroy, id: @product_history
    end

    assert_redirected_to product_histories_path
  end
end
