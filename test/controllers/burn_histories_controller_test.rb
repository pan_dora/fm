require 'test_helper'

class BurnHistoriesControllerTest < ActionController::TestCase
  setup do
    @burn_history = burn_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:burn_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create burn_history" do
    assert_difference('BurnHistory.count') do
      post :create, burn_history: { description: @burn_history.description, firmware_id: @burn_history.firmware_id, operater_id: @burn_history.operater_id, product_id: @burn_history.product_id, scheme_id: @burn_history.scheme_id }
    end

    assert_redirected_to burn_history_path(assigns(:burn_history))
  end

  test "should show burn_history" do
    get :show, id: @burn_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @burn_history
    assert_response :success
  end

  test "should update burn_history" do
    patch :update, id: @burn_history, burn_history: { description: @burn_history.description, firmware_id: @burn_history.firmware_id, operater_id: @burn_history.operater_id, product_id: @burn_history.product_id, scheme_id: @burn_history.scheme_id }
    assert_redirected_to burn_history_path(assigns(:burn_history))
  end

  test "should destroy burn_history" do
    assert_difference('BurnHistory.count', -1) do
      delete :destroy, id: @burn_history
    end

    assert_redirected_to burn_histories_path
  end
end
